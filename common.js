'use strict'

const usersAPI = 'https://ajax.test-danit.com/api/json/users';
const postsAPI = 'https://ajax.test-danit.com/api/json/posts';
const root = document.querySelector('.root');

const sendRequest = (url, method = 'GET') => {
    return fetch(url)
        .then((response) => response.json())
}

let usersList = [];
let postsList = [];

function getUsers() {
    sendRequest(usersAPI)
        .then((data) => {
            usersList = data;
            generationArray();
        })
}

function getPosts() {
    sendRequest(postsAPI)
        .then((data) => {
            postsList = data;
            generationArray();
        })
}

function generationArray() {
    if (usersList.length && postsList.length) {
        const cards = [];
        usersList.forEach(({name, email, id}) => {
            postsList.forEach(({userId, title, body, id: postId}) => {
                if (id === userId) {
                    cards.push(new Card(title, body, name, email, postId));
                    root.insertAdjacentHTML('beforeend', cards[cards.length - 1].renderCard())
                }
            })
        })
    }
}

root.addEventListener('click', (event) => {
    let postId = event.target.id;
    if (event.target.type === 'submit') {
        fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
            method: "DELETE"
        })
            .then((response) => response)
            .then((data) => {
                if (data.status === 200) {
                    event.target.closest('.card').remove();
                }
            })
    }
})

class Card {
    constructor(title, body, name, email, postId) {
        this.title = title;
        this.body = body;
        this.name = name;
        this.email = email;
        this.postId = postId;
    }

    renderCard() {
        return `<div class="card">
                    <button type="submit" id="${this.postId}">x</button>
                    <h3>${this.title}</h3>
                    <h4>${this.body}</h4>
                    <h3>${this.name}</h3>
                    <h4>${this.email}</h4>
                </div>`;
    }
}

getUsers();
getPosts();

